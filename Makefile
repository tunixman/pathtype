run-test:
	runhaskell Setup configure --user -fbuildTools
	runhaskell Setup build
	runhaskell Setup haddock
	runhaskell Setup configure --user --enable-tests
	make update-test
	./dist/build/test/test

update-test:
	./dist/build/create-pathtype-test/create-pathtype-test
	runhaskell Setup build
