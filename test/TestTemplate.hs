{-# OPTIONS_GHC -fno-warn-warnings-deprecations #-}
module TestResult (results) where

import qualified System.Path.PartClass as Class
import qualified System.Path.Generic as Path
import qualified System.Path.Posix as Posix
import qualified System.Path.Windows as Windows
import System.Path.Generic ((</>), (<.>), relFile, relDir, absFile, absDir)
import Data.Char (toLower)


results :: (Class.AbsRel ar) => Char -> Posix.FilePath ar -> [(String, Bool)]
results a x =
  <TESTS_GO_HERE>
  []
