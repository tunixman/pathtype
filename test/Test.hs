module Main (main) where

import TestResult (results)

import qualified System.Path.Posix as Posix
import qualified System.Path.Windows as Windows
import System.Path (rootDir, relDir, relFile, (</>), (<.>))
import System.Random (newStdGen, random)
import System.Exit (exitFailure)
import Control.Monad (void)
import Text.Printf (printf)


main :: IO ()
main = do
  g <- newStdGen
  let (a,_g') = random g
      -- TODO - integrate with QuickCheck
      x = rootDir </> relDir "tmp" </> relFile "someFile" <.> "ext"

  let allResults = results a x
  void $ printf "Running %d tests...\n" $ length allResults

  let fails = map fst $ filter (not . snd) allResults
  if null fails
    then do
      putStrLn "Passed."
    else do
      putStrLn "Failed tests:"
      mapM_ putStrLn fails
      exitFailure

  putStrLn "Running QuickCheck tests ..."
  let runQC prefix =
        mapM_ $ \(name, test) -> putStr (prefix ++ name ++ ": ") >> test
  runQC "Posix." Posix.testAll
  runQC "Windows." Windows.testAll
  putStrLn "Tests completed."
